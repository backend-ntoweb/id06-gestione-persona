package app.nike.clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import app.nike.models.User;

@FeignClient(name = "id05-gestione-utente", path = "/user")
public interface GestioneUtenteFeignClient {

	@PostMapping("/crea")
	public User createUser(@RequestBody User user);
	
	@DeleteMapping("/delete/{username}")
	public void deleteUtente(String username);
	
	@GetMapping("/{username}")
	public User getUserByUsername(@PathVariable String username);
}
