package app.nike.clients;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import app.nike.models.Persona;

@FeignClient(name = "id8-persona-curd", path = "/api/nike")
public interface PersonaCrudFeignClient {

	@PostMapping("/salvaPersona")
    public Persona createPersona (@RequestBody Persona p);


    @PostMapping("/aggiornaPersona")
    public Persona updatePersona(@RequestBody Persona p) ;


    @DeleteMapping("/cancellaPersona/{codiceFiscale}")
    public void  deletePersona(@PathVariable(value = "codiceFiscale") String codiceFiscale);

    @GetMapping("/recuperaPersona/{codiceFiscale}")
    public Persona findByCodiceFiscale(@PathVariable (value = "codiceFiscale") String id);


    @GetMapping("/recuperaPersone")
    public List<Persona> getPersone() ;
}
