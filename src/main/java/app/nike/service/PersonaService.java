package app.nike.service;

import java.util.List;

import app.nike.models.Persona;

public interface PersonaService {

	public Persona getPersona(String codiceFiscale) throws Exception;

	public List<Persona> getPersone() throws Exception;

	public void deletePersona(String codiceFiscale) throws Exception;

	public Persona createPersona(Persona persona) throws Exception;

}
