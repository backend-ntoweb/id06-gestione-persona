package app.nike.service;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.nike.clients.GestioneEmailFeignClient;
import app.nike.clients.GestioneUtenteFeignClient;
import app.nike.clients.PersonaCrudFeignClient;
import app.nike.models.Persona;
import app.nike.models.Role;
import app.nike.models.User;
import app.nike.utility.GenericUtility;
import app.nike.utility.RoleName;

@Service
public class PersonaServiceImpl implements PersonaService {

	@Autowired
	GestioneUtenteFeignClient gestioneUtenteFeign;

	@Autowired
	PersonaCrudFeignClient personaCrudFeign;

	@Autowired
	GestioneEmailFeignClient gestioneEmailFeign;

	@Override
	public Persona createPersona(Persona persona) throws Exception {
		if (personaCrudFeign.findByCodiceFiscale(persona.getCodiceFiscale()) != null) {
			throw new Exception("Codice Fiscale già esistente");
		}
		Persona personaToReturn;
		Role role = new Role();
		role.setName(RoleName.ROLE_PERSONA);
		String randomPass = GenericUtility.randomPassword();
		User user = new User(persona.getCodiceFiscale(), randomPass, persona.getEmail(), true, Arrays.asList(role));

		try {
			if (gestioneUtenteFeign.createUser(user) == null)
				throw new Exception("La creazione dello User non è andata a buon fine");

			personaToReturn = personaCrudFeign.createPersona(persona);

			if (personaToReturn != null) {
				Thread thread = new Thread() {
					public void run() {
						gestioneEmailFeign.inviaEmail(GenericUtility.registrationEmail(
								personaToReturn.getCodiceFiscale(), personaToReturn.getNome(),
								personaToReturn.getCognome(), personaToReturn.getEmail(), randomPass));
					}
				};
				thread.start();
			}
			return personaToReturn;

		} catch (Exception e) {
			throw new Exception("Si sono verificati dei problemi durante il salvattaggio, prova a cambiare la email");
		}
	}

	@Override
	public void deletePersona(String codiceFiscale) throws Exception {
		if (personaCrudFeign.findByCodiceFiscale(codiceFiscale) == null
				&& gestioneUtenteFeign.getUserByUsername(codiceFiscale)==null)
			throw new Exception("Non è presente nessun utente con questo codice Fiscale");
		
		Thread deletePersona = new Thread() {
			public void run() {
				personaCrudFeign.deletePersona(codiceFiscale);
			}
		};
		
		deletePersona.start();
		Thread deleteUtente = new Thread() {
			public void run() {
				gestioneUtenteFeign.deleteUtente(codiceFiscale);
			}
		};
		deleteUtente.start();
		
	}

	@Override
	public List<Persona> getPersone() throws Exception {
		try {
			return personaCrudFeign.getPersone();
		} catch (Exception e) {
			throw new Exception("Qualcosa è andato storno nella ripresa della lista");
		}

	}

	@Override
	public Persona getPersona(String codiceFiscale) throws Exception {
		try {
			return personaCrudFeign.findByCodiceFiscale(codiceFiscale);
		} catch (Exception e) {
			throw new Exception("qualcosa è andato storno nella ripresa della persona");
		}

	}
}
