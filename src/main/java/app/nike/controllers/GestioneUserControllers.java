package app.nike.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import app.nike.models.Persona;
import app.nike.service.PersonaService;

@RestController
@RequestMapping("/persona")
public class GestioneUserControllers {
	
	@Autowired
	private PersonaService personaService;

	@GetMapping("/test")
	public String prova() {
		return "TEST REST CONTROLLER";
	}
	
	@PostMapping("/crea")
	public Persona addPersona(@RequestBody Persona persona) {
		try {
			return personaService.createPersona(persona);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return null;
	}
	
	@DeleteMapping("/delete/{codiceFiscale}")
	public void deletePersona(@PathVariable(value = "codiceFicale")String codiceFiscale) {
		try {
			personaService.deletePersona(codiceFiscale);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	@GetMapping("/getAll")
	public List<Persona> getPersone() {
		try {
			return personaService.getPersone();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return null;
	}
	
	@GetMapping("/{codiceFiscale}")
	public Persona getPersona(@PathVariable(value = "codiceFiscale")String codiceFiscale) {
		try {
			return personaService.getPersona(codiceFiscale);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return null;
	}
}
